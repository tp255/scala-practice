//package akkahttp
//import akka.actor.ActorSystem
//import akka.http.scaladsl.Http
//import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
//import akka.http.scaladsl.model.StatusCodes
//import akka.http.scaladsl.server.Directives._
//import akka.http.scaladsl.server.Route
//import akka.stream.ActorMaterializer
//import akkahttp.UserJsonProtocol.userFormat
//import spray.json.DefaultJsonProtocol._
//import spray.json.RootJsonFormat
//
//case class User(id: Int, name: String, age: Int)
//
//object UserJsonProtocol {
//  implicit val userFormat: RootJsonFormat[User] = jsonFormat3(User)
//}
//
//object UserDatabase {
//  // Define an in-memory database of users
//  private var users: List[User] = List(
//    User(1, "Alice", 25),
//    User(2, "Bob", 30),
//    User(3, "Charlie", 35)
//  )
//
//  def getAllUsers(): List[User] = users
//
//  def getUserById(id: Int): Option[User] = users.find(_.id == id)
//
//  def createUser(user: User): Unit = users = user :: users
//
//  def updateUser(id: Int, user: User): Unit = {
//    users = users.map {
//      case User(`id`, _, _) => user
//      case other => other
//    }
//  }
//
//  def deleteUser(id: Int): Unit = users = users.filterNot(_.id == id)
//}
//
//object UserRoutes {
//  // Define the HTTP routes for our CRUD API
//  val userRoutes: Route = {
//    pathPrefix("users") {
//      get {
//        val users = UserDatabase.getAllUsers()
//        complete(users.toString)
//      } ~
//        path(IntNumber) { id =>
//          get {
//            val user = UserDatabase.getUserById(id)
//            user match {
//              case Some(u) => complete(u.toString)
//              case None => complete(StatusCodes.NotFound)
//            }
//          } ~
//            put {
//              entity(as[User]) { user =>
//                UserDatabase.updateUser(id, user)
//                complete(StatusCodes.NoContent)
//              }
//            } ~
//            delete {
//              UserDatabase.deleteUser(id)
//              complete(StatusCodes.NoContent)
//            }
//        } ~
//        post {
//          entity(as[User]) { user =>
//            UserDatabase.createUser(user)
//            complete(StatusCodes.Created)
//          }
//        }
//    }
//  }
//}
//
//object Main extends App {
//  implicit val system: ActorSystem = ActorSystem("crud-api")
//  implicit val materializer: ActorMaterializer.type = ActorMaterializer
//
//  val bindingFuture = Http().newServerAt("localhost", 7080).bind(UserRoutes.userRoutes)
//}
