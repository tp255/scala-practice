package akkahttp

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.server.Directives.{complete, entity, path, pathEnd, pathPrefix, post}
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, RootJsonFormat, _}
import akka.http.scaladsl.server.Directives._

trait JsonMapping extends SprayJsonSupport with DefaultJsonProtocol{
  implicit val userFormat: RootJsonFormat[User] = jsonFormat4(User)
}

case class User(id: Int, name : String, age: Int, address: String)

object UserService{
  var users  = List[User]()
  def getAllUsers():List[User] = users
  def getUserById(id: Int):Option[User] = {
    users.find(_.id == id)
  }
  def deleteUserById(id: Int): Option[User] = {
    getUserById(id) match {
      case Some(user) =>
        users = users.filterNot(_.id == id)
        Some(user)
      case None =>
        None
    }
  }

  def createUser(user: User): Unit = users = user :: users
}

object UserRoutes extends JsonMapping{
  val route: Route = path("users") {
    get {
      complete(UserService.getAllUsers().map(_.toJson))
    }~
    path(IntNumber) { id =>
      get {
        val user = UserService.getUserById(id)
        user match {
          case Some(user) => complete(user.toJson)
          case None => complete(StatusCodes.NotFound)
        }
      } ~
        delete {
          UserService.deleteUserById(id)
          complete(StatusCodes.NoContent)
        }
    }~
    post {
      entity(as[User]) { user =>
        UserService.createUser(user)
        complete(StatusCodes.Created)
      }
    }
  }
}
object UserCrudApp extends  App {
  implicit val system = ActorSystem("my-system")
  implicit val materializer: ActorMaterializer.type = ActorMaterializer
  val server = Http().newServerAt("localhost", 9090).bind(UserRoutes.route)
}
