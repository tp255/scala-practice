//package akkahttp
//
//import akka.actor.ActorSystem
//import akka.http.scaladsl.Http
//import akka.http.scaladsl.server.Directives
//import akka.http.scaladsl.server.Directives.{complete, path, pathPrefix}
//import akka.stream.{ActorMaterializer, Materializer}
//
//import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
//import akka.http.scaladsl.server.RouteResult._
//
//object HttpServer {
//  def main(args: Array[String]): Unit = {
//    implicit val system: ActorSystem = ActorSystem()
//    implicit val executor: ExecutionContextExecutor = system.dispatcher
//    implicit val materializer: ActorMaterializer = ActorMaterializer()
//    import system.dispatcher
//
//    val route =
//      path("hello") {
//        Directives.get {
//          complete("Hello, World!")
//        }
//      }
//
//    val bindingFuture = Http().newServerAt("localhost", 8080).bind(route)
//  }
//
//}
