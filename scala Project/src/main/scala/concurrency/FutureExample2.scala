import scala.concurrent.{Await, Future, duration}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

object FutureExample2 extends App {
  val random = new Random()

  def performTask(taskId: Int): Future[Int] = Future {
    // Simulate a long-running task
    Thread.sleep(random.nextInt(1000))
    println(s"Task $taskId completed")
    taskId
  }

  val tasks = Seq(
    performTask(1),
    performTask(2),
    performTask(3),
    performTask(4),
    performTask(5)
  )

  val aggregatedResult = Future.sequence(tasks).map(_.sum)

  aggregatedResult.onComplete {
    case scala.util.Success(result) => println(s"Aggregated result: $result")
    case scala.util.Failure(exception) => println(s"Failed: $exception")
  }

  // Do some other work
  println("Doing some other work")

  // Block until the future completes
  val result = Await.result(aggregatedResult, duration.Duration.Inf)
  println(s"Final result: $result")
}
