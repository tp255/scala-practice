package concurrency
import scala.concurrent.{Await, Future, duration}
import scala.concurrent.ExecutionContext.Implicits.global

object FutureExample extends App {
  val future = Future {
    // Some long-running operation
    Thread.sleep(1000)
    42
  }

  future.onComplete {
    case scala.util.Success(result) => println(s"Result: $result")
    case scala.util.Failure(exception) => println(s"Failed: $exception")
  }

  // Do some other work
  println("Doing some other work")

  // Block until the future completes
  val result = Await.result(future, duration.Duration.Inf)
  println(s"Final result: $result")
}
