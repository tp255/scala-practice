## Companion Object features

1. Can be thought as the container of static fields and methods of a class as scala does not **static** keyword. Anything inside it is static by default
2. A class/trait named same is called its companion class/trait ( but should be present in the same file )
3. Has access to private fields and methods of the companion class/trait
4. **object** keyword is used
5. **object** keyword can also be used to create singleton class i.e. without the companion class/trait as standalone
6. Also known as singleton object
7. Case classes and case objects automatically create companion object internally
8. Can have factory method to create the instance of the class ( apply method )

## Common doubts/questions

#### 1. Is it is class or object? And in which way it is related to companion class/trait?

**Ans:** Internally the compiler creates a class ( with $ sign ) for its own usage and returns its instance. The class is singleton in nature. The $ sign class is created at compile time, and it's instance is returned at runtime when it is first accessed. So, internally companion object and companion class/trait is to different entities bound by above features
## To remember

```
object Demo {
}
```

### Together with companion class ( in same file )

```
class Demo1 {
  private val x = 5;
}

object Demo1 {
  // we can access x here
  println(x);
}
```

### Apply method

```
class Demo2(val x: Int, val y: Int)

object Demo2 {
  def apply(x: Int, y: Int): Demo2 = new Demo2(x, y);
}
 
val obj = Demo2(4, 5); // creating instance
```
