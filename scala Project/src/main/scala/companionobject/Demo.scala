package companionobject

// Companion class accessing the private member of the companion object
class Demo {
  private def myPrint():Unit = {
    println(Demo.x)
  }
}

object Demo {
  private val x = 5

  def main(args: Array[String]): Unit = {
    var d = new Demo()
    d.myPrint();
  }
}
