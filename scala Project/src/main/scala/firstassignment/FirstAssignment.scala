package firstassignment

import scala.+:
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FirstAssignment {
  case class Info(address: String, emailOpt: Option[String])

  case class User(id: Option[Long], name: String, ageOpt: Option[Int], infoOpt: Option[Info] = None)

  object User {
    def empty: User = User(None, "empty", None)
  }

  val user1: User = User(None, "name1", ageOpt = Some(22), Some(Info("address1", None)))
  val user2: User = User(Some(1), "name2", ageOpt = None)
  val user3: User = User(Some(2), "name3", Some(0), Some(Info("", Some("email1"))))
  val user4: User = User(Some(3), "name4", Some(35), Some(Info("address2", Some("email2"))))
  val user5Opt: Option[User] = Some(User(Some(4), "name5", ageOpt = Some(45)))
  val user6Opt: Option[User] = None

  val seq1: Seq[User] = Seq(user1, user2, user3, user4)

  val r = scala.util.Random

  def getUser: Future[Option[User]] = {
    val value = r.nextInt(8)
    Future(seq1.lift(value))
  }

  def main(args: Array[String]): Unit = {
    val ans11: Seq[String] = seq1.map(user => user.name)
    val ans12: Seq[Int] = seq1.map(user => user.ageOpt.getOrElse(0))
    val ans13: Seq[User] = seq1.filter(user => user.ageOpt.getOrElse(0) > 0)
    val ans131: Seq[User] = seq1.filter(user => user.ageOpt.getOrElse(0) > 0 ||  user.ageOpt == None)
//    val ans14: Seq[User] = seq1.map(user => if (user.ageOpt.getOrElse(None) == 0)  else None  )
    val ans15 : Int = seq1.map(_.ageOpt.getOrElse(0)).sum
    val ans16 : String = seq1.map(_.name).mkString(",")

    val ans17 = scala.collection.mutable.Map[Long, String]()
    seq1.foreach{
      user => ans17.put(user.id.getOrElse(0), user.name)
    }
    val ans18Forward : Seq[User]  = seq1.sortBy(_.ageOpt)
    val ans18Reverse : Seq[User]  = seq1.sortBy(_.ageOpt).reverse

    val user7 : User =  User(Some(7), "", Some(55))
    val seq2: Seq[User]  = (user7 +: seq1) :+ user7

    val (ageLess30, ageGreater30) = seq2.partition(_.ageOpt.getOrElse(0) < 30)

    val ans113 : String = seq1.map(_.infoOpt.map(_.address)).mkString(",")

//    val userEmail2 : Seq[Option[Info]] = seq1.filter(user => user.infoOpt.filter(_.emailOpt.getOrElse("").equals("email2")).getOrElse(User.empty))

    val ans115 : Int = seq1.map(_.ageOpt.getOrElse(0)).foldLeft(0)(_ + _)


    println(ans11)
    println(ans12)
    println(ans13)
    println(ans131)
    println(ans15)
    println(ans16)
    println(ans17)
    println(ans18Forward)
    println(ans18Reverse)
    println(user7)//ans1.9
    println(seq2)//ans1.10
    println(ageLess30)//ans1.11
    println(ageGreater30)//ans1.11
//    println(userEmail2)//ans1.14
    println(ans115)//ans1.15

  }


  //1.
  //1.1 get a collection with usernames from collection seq1
  //1.2 get a collection with ages Seq[Int] from collection seq1
  //1.3 need to get Seq[User] collection from seq1 where age > 0
  //1.3.1 need to get Seq[User] collection from seq1 where age > 0 || age is None
  //1.4 for all users of the collection seq1 replace age where age = 0 from age = 0 to age = 5
  //1.5 get sum of all ages of users from seq1
  //1.6 print all usernames in one line separated by commas
  //1.7 create Map[Long, String] where Long is id and String is username
  //1.8 sort seq1 by age in forward and reverse order
  //1.9 create user7 using User.empty (without name, id = 7, age = 55)
  //1.10 add user7 to the beginning and to the end of seq1
  //1.11 divide the collection into 2 parts in the first age > 30 the second age < 30
  //1.13 print all addresses in one line separated by commas
  //1.14 find the User with email = email2 if there isn't then output User.empty
  //1.15 use foldLeft to get sum of all ages

  //2. use getUser for rest tasks (to output the result use this construction (1 to 10).foreach(_ => Await.result(YourFunctionHere, 1.seconds)) )
  //2.1 print username if it doesn't exist print "user does not exist"
  //2.2 print email, if email doesn't exists then print "email does not exists", if user doesn't exists then print "user does not exists"

  //3
  //3.1 create your git accaunt
  //3.2 add to Git your solution

  //4 Implicits
  //4.1 Create an implicit function so that this expression compiles without errors
  //  val nuberOfLetters: Int = "some string"
  //4.2 Create an implicit class with str2int func so that this expression compiles without errors
  //  val nuberOfLetters: Int = "some string".str2int
}
