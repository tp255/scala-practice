package traits
/*
* traits is similar as java interface
* traits is combination of abstract and non-abstract
* */
trait Calculator {
  //non-abstract class
  def add(num1:Int, num2:Int) = { num1+num2 }

  def mul(num1: Int, num2: Int): Int = {
    num1 * num2
  }

  //abstract class
  def subtract(num1:Int, num2:Int): Int
  def div(num1:Int, num2:Int)
}



object CalculatorRun {

}


