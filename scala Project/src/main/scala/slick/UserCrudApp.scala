package slick

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.server.Directives.{complete, entity, path, pathEnd, pathPrefix, post}
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, RootJsonFormat, _}
import akka.http.scaladsl.server.Directives._
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global


trait JsonMapping extends SprayJsonSupport with DefaultJsonProtocol{
  implicit val userFormat: RootJsonFormat[User] = jsonFormat4(User)
}

object UserRoutes extends JsonMapping{
  val route: Route = path("users") {
    get {
      complete(UsersDao.findAll().map(_.toJson))
    }~
    path(IntNumber) { id =>
      get {
        complete(UsersDao.findById(id).map(_.toJson))
      } ~
      delete {
        complete(UsersDao.delete(id).map(_.toJson))
      }
    }~
    post {
      entity(as[User]) { user =>
        complete(UsersDao.create(user).map(_.toJson))
      }
    }
  }
}

object UserCrudApp extends  App {
  val db = Database.forConfig("postgres")
  implicit val system = ActorSystem("my-system")
  implicit val materializer: ActorMaterializer.type = ActorMaterializer
  val server = Http().newServerAt("localhost", 9090).bind(UserRoutes.route)
}
