package slick

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
object UsersDao extends BaseDao {
  def findAll(): Future[Seq[User]] = userTable.result

  def findById(id: Long): Future[User] = userTable.filter(_.id === id).result.head

  def create(user: User): Future[Long] = userTable returning userTable.map(_.id) += user

  def delete(id: Long): Future[Int] = userTable.filter(_.id === id).delete
}