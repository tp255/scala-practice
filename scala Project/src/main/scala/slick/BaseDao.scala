package slick

import slick.dbio.NoStream
import slick.jdbc.JdbcBackend.Database
import slick.sql.{FixedSqlStreamingAction, SqlAction}

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._
trait BaseDao {
  val db = Database.forConfig("postgres")
  implicit val session: Session = db.createSession()

  val userTable: TableQuery[UserTable] = TableQuery[UserTable]

  protected implicit def executeFormDb[A](action: SqlAction[A, NoStream, _ <: slick.dbio.Effect]): Future[A] = {
    db.run(action)
  }

  protected implicit def executeReadStreamFormDb[A](action: FixedSqlStreamingAction[Seq[A], A, _ <: slick.dbio.Effect]): Future[Seq[A]] = {
    db.run(action)
  }
}