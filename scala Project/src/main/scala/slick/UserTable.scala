package slick

import slick.jdbc.PostgresProfile.api._
class UserTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey)
    def name = column[String]("name")
    def age = column[Int]("age")
    def address = column[String]("address")
    override def * = (id, name, age, address) <> (User.tupled, User.unapply)
  }

