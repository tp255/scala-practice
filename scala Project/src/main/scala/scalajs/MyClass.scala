package scalajs
import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("MyClass")
object MyClass {
  def main(args: Array[String]): Unit = {
    def concat(a: String, b: String): String = a + b
  }

}