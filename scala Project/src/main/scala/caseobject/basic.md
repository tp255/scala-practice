## Case Object features

1. Case object is internally a singleton class with one instance created at the compile time
2. Internally companion object is created
3. Case object also have features of case classes like ( useful in pattern matching, immutable, have built in methods like
   toString, hashCode, apply, etc., and serializable by default )

## Common doubts/questions
#### Q1: When to use case object and how it is different from case class?

### Declaration

```
case object Demo
```

### Declaring methods

```
case object Demo2 {
  def someMethod(): Unit = {
  }
  val someProperty: Int = 1
}
```

