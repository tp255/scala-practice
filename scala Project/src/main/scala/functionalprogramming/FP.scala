package functionalprogramming

object FP {

  def main(args: Array[String]): Unit = {
    val additionResult = performArithmeticOperation(2, 4, "addition")
    assert(additionResult == 6)

    val subtractionResult = performArithmeticOperation(10, 6, "subtraction")
    assert(subtractionResult == 4)

    val multiplicationResult = performArithmeticOperation(8, 5, "multiplication")
    assert(multiplicationResult == 40)

    println(additionResult)
    println(subtractionResult)
    println(multiplicationResult)
  }
  def performAddition(x: Int, y: Int): Int = x + y

  def performSubtraction(x: Int, y: Int): Int = x - y

  def performMultiplication(x: Int, y: Int): Int = x * y


  def performArithmeticOperation(num1: Int, num2: Int, operation: String): Int = {
    operation match {
      case "addition" => performAddition(num1, num2)
      case "subtraction" => performSubtraction(num1, num2)
      case "multiplication" => performMultiplication(num1, num2)
      case _ => -1
    }
  }


}
