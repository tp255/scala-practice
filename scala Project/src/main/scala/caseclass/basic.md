## Case Class features

1. Immutability: Once the instance of case class created its state can not be changed
2. Automatic constructor parameters: A case class automatically generates constructor parameters for all of its
   properties
3. Automatic toString, hashCode, copy, and equals methods: Utility methods
4. Companion objects: A case class automatically generates a companion object that has the same name as the class
5. Pattern matching: Case classes are commonly used in pattern matching
6. Serializable: Case classes are automatically serializable

## Common doubts/questions

#### Q1: What is difference between the normal class and case class

**Ans:** Case class is different because:

1. It is optimized for pattern matching
2. Automatically generating a companion object with apply and unapply methods
3. Automatically generating a "toString", "equals" and "hashCode" methods that compare all the fields of the case class
4. Immutable by default

### Declaration

```
case class Demo()
```

### Declaration with parameters ( a and b are also the instance variables )

```
case class Demo2(a: Int, b: String)
```

### Creating new instances

```
case class Demo3(a: String, b: String)

val obj = Demo3("Hello", "World");
```