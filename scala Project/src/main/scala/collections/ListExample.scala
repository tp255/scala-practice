package collections

object ListExample {
  def main(args: Array[String]) {
    val fruit1: List[String] = List("apples", "oranges", "pears")
    val fruit2: List[String] = List("mangoes", "banana")

    // use two or more lists with ::: operator
    var fruit = fruit1 ::: fruit2
    println( "fruit1 ::: fruit2 : " + fruit )

    // use two lists with Set.:::() method
    fruit = fruit1.:::(fruit2)
    println( "fruit1.:::(fruit2) : " + fruit )

    // pass two or more lists as arguments
    fruit = List.concat(fruit1, fruit2)
    println( "List.concat(fruit1, fruit2) : " + fruit  )

    val fruit3 = List.fill(3)("apples") // Repeats apples three times.
    println("fruit : " + fruit)

    val num = List.fill(10)(2) // Repeats 2, 10 times.
    println("num : " + num)

    val squares = List.tabulate(6)(n => n * n)
    println("squares : " + squares)
  }
}
