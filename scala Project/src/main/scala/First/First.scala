package First

object First {
  def main(args: Array[String]): Unit = {
    println(check30(31, 1))
  }

  def test(x:Int, y:Int) : Int =
  {
    if (x == y) (x + y) * 3 else (x + y)
  }

  private def check30(x:Int, y:Int) : Boolean =
  {
    if(x == 30 || y == 30 || (x + y) == 30) true else false
  }
}
