package actor


import akka.actor.{Actor, ActorSystem, Props}

import scala.concurrent.ExecutionContext.Implicits.global

case object Pong
case object Ping
class MyActor extends Actor {
  def receive = {
    // When the actor receives a Greeting message, it prints out the message
    case Pong =>
      println("Pong")
      Thread.sleep(1000)
      sender() ! Ping

    case Ping =>
      println("Pong")
      Thread.sleep(1000)
      sender() ! Pong

  }
}

object Mains extends App {

  val system = ActorSystem("MyActor")
  val pongActor = system.actorOf(Props[MyActor], name = "pong")
  val pingActor = system.actorOf(Props[MyActor], name = "ping")

  pingActor ! Pong

  system.scheduler.scheduleOnce(scala.concurrent.duration.Duration(1, "second")) {
    system.terminate()
  }
}