package optionsandeither

object optionExample {
  def main(args: Array[String]): Unit = {
    // defining a function that returns an option
    def divide(dividend: Double, divisor: Double): Option[Double] = {
      if (divisor == 0) None
      else Some(dividend / divisor)
    }

    // using the function
    val result1 = divide(6, 3) // returns Some(2.0)
    val result2 = divide(6, 0) // returns None

    // pattern matching on options to extract the value or handle the absence of a value
    result1 match {
      case Some(value) => println(s"The result is $value")
      case None => println("No result found")
    }

    // using the map function to transform the value inside an option
    val result3 = result1.map(_ * 2) // returns Some(4.0)

    // using the flatMap function to chain operations that return options
    val result4 = result1.flatMap(value => divide(value, 2)) // returns Some(1.0)

    // using the getOrElse function to provide a default value if the option is None
    val result5 = result2.getOrElse(0.0) // returns 0.0

  }
}
