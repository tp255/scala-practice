package optionsandeither

object EitherExample {
  def main(args: Array[String]): Unit = {
    // defining a function that returns an Either
    def divide(dividend: Double, divisor: Double): Either[String, Double] = {
      if (divisor == 0) Left("Divisor cannot be zero")
      else Right(dividend / divisor)
    }

    // using the function
    val result1 = divide(6, 3) // returns Right(2.0)
    val result2 = divide(6, 0) // returns Left("Divisor cannot be zero")

    // pattern matching on Either to handle the left or right value
    result1 match {
      case Right(value) => println(s"The result is $value")
      case Left(error) => println(s"Error: $error")
    }

    // using the map function to transform the right value inside an Either
    val result3 = result1.map(_ * 2) // returns Right(4.0)

    // using the flatMap function to chain operations that return Eithers
    val result4 = result1.flatMap(value => divide(value, 2)) // returns Right(1.0)

    // using the fold function to handle both the left and right values at once
    val result5 = result2.fold(
      error => println(s"Error: $error"),
      value => println(s"The result is $value")
    )

  }
}
