## Pattern Matching features

1. Pattern matching is powerful feature in scala because it is not limited to strings, it can be used to match pattern
   of case classes, traits, types, sequences, etc.
2. Keywords used are switch, case, _ ( AKA wildcard to cover default case )
3. Pattern matching can think as switch case in java

## Common doubts/questions

#### 1. Why we have pattern matching? what problem it solves?

**Ans:** Compared to java first it reduces verbosity and second it add extra features. In Java, we use switch-case or if-else but here same logic is in less line of code. Apart from that java only supports pattern matching of strings with help of external library

## To remember

1. The cases should cover all the case. Otherwise, error/warning

### Constant/Variable Matching

```
val x = 5
x match {
  case 1 => println("one")
  case _ => println("other")    //_ ( wildcard ) to represent default case
}
```

### Type matching

```
val x: Any = "hello"
x match {
  case s: String => println(s"x is a String: $s")
  case i: Int => println(s"x is an Int: $i")
  case _ => println("x is some other type")
}
```

### Case class Matching

```
case class Person(name: String, age: Int)
val p = Person("John", 25)
p match {
  case Person("John", 25) => println("Found John, age 25")
  case _ => println("Other person") 
}
```

### Trait matching

```
sealed trait Shape  //sealed trait is a trait that can only be extended in the same file
case class Circle(radius: Double) extends Shape
case class Rectangle(width: Double, height: Double) extends Shape

val s: Shape = Circle(5.0)
s match {
  case Circle(r) => println(s"Circle with radius $r")   //s is used for string interpolation
  case Rectangle(w, h) => println(s"Rectangle with width $w and height $h")
}
```

### Pattern guard matching

###### <small> Pattern guard is extra condition to be true before executing the case </small>

```
val x = 5
x match {
  case a if a > 0 && a < 10 => println("x is between 0 and 10")
  case _ => println("x is outside the range")
}
```

### Tuples matching

```
val t = (1, 2, 3)
t match {
  case (1, x, y) => println(s"Found 1, x: $x, y: $y")
  case (2, x, y) => println(s"Found 2, x: $x, y: $y")
  case _ => println("other")
}
```

### Other

We can also match sequences, enumeration, option, etc.

