## Inheritance features

1. Concept is almost similar to Java but syntax is different
2. Here we use `extends` keyword to both extend class or implement a trait
3. Here also multiple inheritance is only possible through traits. Here we use `with` keyword to implements many traits

## Common doubts/questions

##### Q1: What is difference between the Java and Scala inheritance?

**Ans:** Differences:

1. In Java all classes are inherited from `Object` class, but in scala all classes are inherited from `Any` class
2. In Scala `override` keyword is required when overriding, but it is optional in Java
3. Some other differences might be present apart from the keywords used

### Syntax to extend class

```
class ChildClass extends ParentClass {
    // class body
}
```

### Syntax to implement trait

```
class ChildClass extends Trait {
    // class body
}
```

### Syntax to implement multiple traits

```
class ChildClass extends Trait1 with Trait2 with Trait3 {
    // class body
}
```

### Override the method

```
// Parent class
class ParentClass {
    def sum(x: Int, y: Int) {
        return x - y;
    }
}

// Child class
class ChildClass extends ParentClass {
    
    override def sum(x: Int, y: Int) {  // override keyword is required
        x + y;
    }
}
```