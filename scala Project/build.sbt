import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.11"

enablePlugins(ScalaJSPlugin)
scalaJSUseMainModuleInitializer := true

libraryDependencies ++= {
  val akkaVersion = "2.6.16"
  val akkaHttpVersion = "10.2.6"
  val scalaTestVersion = "3.0.1"
  val scalaMockV = "3.5.0"
  val slickVersion = "3.2.1"
  Seq(
    "com.typesafe.akka"  %% "akka-actor"                  % akkaVersion,
    "com.typesafe.akka"  %% "akka-stream"                 % akkaVersion,

    "com.typesafe.akka"  %% "akka-http"                   % akkaHttpVersion,
    "com.typesafe.akka"  %% "akka-http-core"              % akkaHttpVersion,
    "com.typesafe.akka"  %% "akka-http-spray-json"        % akkaHttpVersion,
    "com.typesafe.akka"  %% "akka-http-testkit"           % akkaHttpVersion % Test,

    "com.typesafe.slick" %% "slick"                       % slickVersion,
    "com.typesafe.slick" %% "slick-hikaricp"              % slickVersion,
    "org.slf4j"           % "slf4j-nop"                   % "1.7.25",
    "org.postgresql"      % "postgresql"                  % "42.1.4",
    "org.flywaydb"        % "flyway-core"                 % "3.2.1",

    "org.scalatest"      %% "scalatest"                   % scalaTestVersion % Test,
    "org.scalamock"      %% "scalamock-scalatest-support" % scalaMockV % Test
  )
}


lazy val root = (project in file("."))
  .settings(
    name := "ScalaPractice"

  )
