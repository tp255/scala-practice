package csvreader

import scala.concurrent.Future

trait StatementSummery {
  def calculateWithdrawAmt( rows:List[Array[String]]) : Future[Double];
  def calculateWithdrawCnt( rows:List[Array[String]]) : Option[Int] ;

  def calculateDepositAmt( rows:List[Array[String]]) : Future[Double];
  def calculateDepositCnt( rows:List[Array[String]]) : Either[String, Int];
}
