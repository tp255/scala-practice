package csvreader

import csvreader.StatementReader.{DEPOSIT_COLUMN, WITHDRAW_COLUMN}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.DurationInt
import scala.io.Source
import scala.util.{Failure, Success, Try}

class StatementReader extends StatementSummery {

  override def calculateWithdrawAmt(rows: List[Array[String]]): Future[Double] = Future {
    var amt :Double = 0;
    rows.foreach(row => {
      if(row(WITHDRAW_COLUMN) != null  && !row(WITHDRAW_COLUMN).isEmpty && row(WITHDRAW_COLUMN).trim != "")
        amt = amt + row(WITHDRAW_COLUMN).toDouble
    })
    Thread.sleep(2000)
    amt
  }

  override def calculateWithdrawCnt(rows: List[Array[String]]): Option[Int] = {
    var cnt: Int = 0;
    rows.foreach(row => {
      if(row(WITHDRAW_COLUMN) != null  && !row(WITHDRAW_COLUMN).isEmpty && row(WITHDRAW_COLUMN).trim != "")
        cnt = cnt +1
    })
    if (cnt == 0) None
    else Some(cnt)
  }

  override def calculateDepositAmt(rows: List[Array[String]]): Future[Double] =  Future {
    var amt: Double = 0;
    rows.foreach(row => {
      if (row(DEPOSIT_COLUMN) != null && !row(DEPOSIT_COLUMN).isEmpty && row(DEPOSIT_COLUMN).trim != "")
        amt = amt + row(DEPOSIT_COLUMN).toDouble
    })
    Thread.sleep(5000)
    amt
  }

  override def calculateDepositCnt(rows: List[Array[String]]): Either[String, Int] = {
    var cnt: Int = 0;
    rows.foreach(row => {
      if (row(DEPOSIT_COLUMN) != null && !row(DEPOSIT_COLUMN).isEmpty && row(DEPOSIT_COLUMN).trim != "")
        cnt = cnt + 1
    })
    if(cnt != 0) Right(cnt)
    else Left("No entries found")
  }
}

object StatementReader
{
  private var DEPOSIT_COLUMN: Int = 5;
  private var WITHDRAW_COLUMN: Int = 4;

  def main(args: Array[String]): Unit = {
    val file = {
      Source.fromFile(getClass.getClassLoader.getResource("csvreader/testing.csv").getPath)
    }

    val rows = file.getLines().toList.map(line => line.split(","))

    var summary = new StatementReader();

    val withdrawAmtResult:Future[Double] = summary.calculateWithdrawAmt(rows)
    println("Total Withdrawal Amt: "+withdrawAmtResult)
    withdrawAmtResult.onComplete{
      case Success(value) => println(s"withdrawal amount after completing future "+ value.toString)
      case Failure(ex) => println("Failed to calculate withdrawal amount")
    }

    summary.calculateWithdrawCnt(rows) match {
      case Some(value) => println(s"The result is $value")
      case None => println("No result found")
    }

    val depositAmtResult: Future[Double] = summary.calculateDepositAmt(rows)
    println("Total Deposit Amt: " + depositAmtResult)
    depositAmtResult.onComplete {
      case Success(value) => println(s"deposit amount after completing future " + value.toString)
      case Failure(ex) => println("Failed to calculate withdrawal amount")
    }

    summary.calculateDepositCnt(rows) match {
      case Right(value) => println(s"The result is $value")
      case Left(value) => println(value)
    }

//    println("Total Withdrawal Amt: "+Await.result(summary.calculateWithdrawAmt(rows), 5.seconds))
//
//    println("Total Deposit Amt: "+Await.result(summary.calculateDepositAmt(rows), 5.seconds))

    file.close()

    Thread.sleep(10000)
  }

}
