import japgolly.scalajs.react.extra.router.{BaseUrl, Redirect, Resolution, Router, RouterConfigDsl, RouterCtl, StaticDsl}
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom
import scalacss.DevDefaults._


object Main extends App {
  sealed trait Page
  case object Home extends Page
  case object About extends Page
  case class BlogPost(id: Int) extends Page

  object Styles extends StyleSheet.Inline {

    import dsl._

    val navBar = style(
      display.flex,
      alignItems.center,
      backgroundColor.rgb(100, 100, 100),
      color.white,
      height(3.em),
      paddingLeft(1.em),
      paddingRight(1.em)
    )

    val navLink = style(
//      padding(1.em),
      textDecoration := "none",
      color.inherit,
      fontWeight._500,
      &.hover(
        textDecoration := "underline"
      )
    )
  }

  val routerConfig = RouterConfigDsl[Page].buildConfig { dsl =>
    import dsl._

    (emptyRule
      | staticRoute(root, Home) ~> render(<.h1("Home"))
      | staticRoute("#about", About) ~> render(<.h1("About"))
      | dynamicRouteCT("#blog" / int.caseClass[BlogPost]) ~> { case BlogPost(id) =>
      render(<.h1(s"Blog post $id"))
    }
      ).notFound(redirectToPage(Home)(Redirect.Replace))
      .renderWith(layout)
  }

  val baseUrl = BaseUrl(dom.window.location.href.takeWhile(_ != '#'))

  def layout(c: RouterCtl[Page], r: Resolution[Page]) =
    <.div(
      <.nav(
        <.ul(
          <.li( c.link(Home)("Home")),
          <.li( c.link(About)("About"))
        )
      ),
      r.render()
    )

  val router = Router(baseUrl, routerConfig.logToConsole)

  router().renderIntoDOM(dom.document.getElementById("app-container"))
}
