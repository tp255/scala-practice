import java.nio.file.Paths
import scala.io.{Source, StdIn}
import scala.util.matching.Regex

object FileReader extends App {
  private val newLogPattern = """\d{2}:\d{2}:\d{2},\d{3}""".r
  private val causedByPattern = """(?i)Caused By:""".r // case insensitive

  private val filename = Paths.get("src/main/scala").toAbsolutePath.normalize.toString + "/server_log.txt"

  println("Enter choice: \n  1. Automatic searching of Error logs \n  2. Manual Searching term")
  private val choice = StdIn.readInt()

  private var term = "ERROR"
  if (choice == 2) {
    println("Enter term to search")
    term = StdIn.readLine()
  }
  else if (choice != 1) {
    println("Invalid choice")
    sys.exit()
  }

  private var isLogFound = false
  private var isNewLog = true
  private var linesPrinted = 0

  for (line <- Source.fromFile(filename).getLines()) {
    if (choice == 1) {
      isNewLog = searchPattern(line, newLogPattern)
      if (isNewLog) {
        linesPrinted = 0
        isLogFound = false
      }

      // Log found condition
      if (isNewLog && line.contains(term)) {
        println("\n" + line)
        isLogFound = true
        linesPrinted = linesPrinted + 1
      }
      // Log print condition
      else if (isLogFound) {
        if (linesPrinted < 3 && linesPrinted > 0) {
          println(line)
          linesPrinted = linesPrinted + 1
          if (linesPrinted == 3) {
            println("...")
          }
        }
        // "Caused by" print condition
        else {
          if (searchPattern(line, causedByPattern)){
            println(line)
            linesPrinted = 1
          }
        }
      }
    }
    else {
      // Manually searching terms
      if (line.contains(term)) {
        println(line + "\n")
      }
    }
  }

  private def searchPattern(line: String, pattern: Regex): Boolean = {
    val matched = pattern.findFirstIn(line)

    matched match {
      case Some(_) => true
      case None => false
    }
  }
}
