## Project description

1. Process log file and find the exception/severe/error logs using pattern matching
2. Process the obtained output and store it in the new file
3. New file will contain all the potential error logs

## Concepts used

1. Basic concepts like `class`, `object`, `case class`, `trait`, etc.
2. `Regular expression` and `Pattern matching` for pattern searching in log file
3. File I/O
4. Concurrent programming by using `Future` and `Actors` when reading large log files
5. Error handling with `Either` and `Try`
6. Functional Programming concepts