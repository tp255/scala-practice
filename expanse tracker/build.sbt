lazy val root = (project in file("."))
  .settings(
    name := "expense tracker",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "2.12.11"
  )

lazy val core = (project in file("core"))
  .settings(
    libraryDependencies ++= Seq(
      "org.endpoints4s" %% "algebra" % "1.9.0",
    )
  )

lazy val server = (project in file("server"))
  .settings(
    libraryDependencies ++= {
      val akkaVersion = "2.6.16"
      val akkaHttpVersion = "10.2.6"
      val scalaTestVersion = "3.0.1"
      val scalaMockV = "3.5.0"
      val slickVersion = "3.2.1"
      Seq(
        "com.typesafe.akka" %% "akka-actor" % akkaVersion,
        "com.typesafe.akka" %% "akka-stream" % akkaVersion,

        "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
        "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
        "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
        "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,

        "com.typesafe.slick" %% "slick" % slickVersion,
        "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
        "org.slf4j" % "slf4j-nop" % "1.7.25",
        "org.postgresql" % "postgresql" % "42.1.4",
      )
    }
  ).dependsOn(core)

lazy val client = (project in file("client"))
  .settings(
      name := "client",
      scalaVersion := "2.13.10",
      scalaJSUseMainModuleInitializer := true,
      libraryDependencies ++= Seq(
        "com.github.japgolly.scalajs-react" %%% "core" % "2.1.1",
        "com.github.japgolly.scalajs-react" %%% "extra" % "2.1.1",
      ),
      jsDependencies ++= Seq(
        "org.webjars.bower" % "react" % "15.3.2" / "react-with-addons.js" minified "react-with-addons.min.js" commonJSName "React",
        "org.webjars.bower" % "react" % "15.3.2" / "react-dom.js" minified "react-dom.min.js" dependsOn "react-with-addons.js" commonJSName "ReactDOM",
        "org.webjars.bower" % "react" % "15.3.2" / "react-dom-server.js" minified "react-dom-server.min.js" dependsOn "react-dom.js" commonJSName "ReactDOMServer"
      ),
  )
  .enablePlugins(ScalaJSPlugin, JSDependenciesPlugin)
  .dependsOn(core)
