package dao


import slick.jdbc.PostgresProfile.api._
import slick.sql.{FixedSqlStreamingAction, SqlAction}
import tables.{ExpenseTable, UserTable}

import scala.concurrent.Future

trait BaseDao {
  val db = Database.forConfig("postgres")
  implicit val session: Session = db.createSession()

  val userTable: TableQuery[UserTable] = TableQuery[UserTable]
  val expenseTable: TableQuery[ExpenseTable] = TableQuery[ExpenseTable]

  protected implicit def executeFormDb[A](action: SqlAction[A, NoStream, _ <: slick.dbio.Effect]): Future[A] = {
    db.run(action)
  }

  protected implicit def executeReadStreamFormDb[A](action: FixedSqlStreamingAction[Seq[A], A, _ <: slick.dbio.Effect]): Future[Seq[A]] = {
    db.run(action)
  }
}
