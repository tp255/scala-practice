package dao

import pojo.Expense

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._

object ExpenseDao extends BaseDao {
  def findAll(): Future[Seq[Expense]] = expenseTable.result

  def findById(id: Long): Future[Expense] = expenseTable.filter(_.id === id).result.head

  def create(expense: Expense): Future[Long] = expenseTable returning expenseTable.map(_.id) += expense

  def delete(id: Long): Future[Int] = expenseTable.filter(_.id === id).delete
}
