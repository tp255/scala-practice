package tables


import pojo.Expense
import slick.jdbc.PostgresProfile.api._
class ExpenseTable(tag: Tag) extends Table[Expense](tag, "expense"){
  def id = column[Long]("id", O.PrimaryKey)
  def userId = column[Long]("user_id")
  def amount = column[Double]("amount")
  def desc = column[String]("desc")
  override def * = (id, userId, amount, desc) <> (Expense.tupled, Expense.unapply)
}
