package tables

import pojo.User
import slick.jdbc.PostgresProfile.api._

class UserTable (tag: Tag) extends Table[User](tag, "user"){
  def id = column[Long]("id", O.PrimaryKey)

  def username = column[String]("username")

  def password = column[String]("password")

  override def * = (id, username, password) <> (User.tupled, User.unapply)
}
